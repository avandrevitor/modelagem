# DESAFIO 01

Modelar um sistema que atenda os requisitos e regras descritas abaixo.

### Problema

Um jovem programador acaba de levar seu automóvel para sua primeira revisão em um centro automotivo, após passar por uma
revisão criteriosa, foi constatado que seu automóvel precisará de manutenção corretiva e algumas preventivas. Após 
descobrir que a manutenção completa irá demorar alguns dias para ficar pronta o jovem decidiu usar um aplicativo chamar 
um táxi e então voltar para sua residência. 

Já em casa, o jovem programador ficou imaginando que  poderia facilmente substituir seu automóvel por um uso mais 
continuo de aplicativos similares ao que acabará de usar. Foi então que pensou, será que a troca compensa ? 


### Objetivo

Modele um sistema que seja capaz de predizer, a partir de algumas informações, se é vantajoso ou não a troca de um 
automóvel pelo hábito do uso de aplicativos de automóvel compartilhado.


### Requisitos Funcionais

1. Sistema deve ser capaz de calcular a partir das caracteristicas do automóvel os valores de seguro, impostos, depreciação, consumo médio de combustivel km/litro, manutenção preventiva;
2. Sistema deve ser capaz de simular os rendimentos do valor do automóvel baseado em uma aplicação de renda fixa;
3. Sisema deve ser capaz de calcular km por dia; ( opcional, usuário do sistema pode informar quantidade km por dia caso o calculo não atenda suas necessidades);
4. Sistema deve ser capaz de considerar o custo de estacionamento;
5. Sistema deve ser capaz de considerar o valor de multas;
6. Sistema deve ser capaz de calcular a partir do consumo de km o custo por aplicativo;
7. Sistema deve ser capaz de calcular tempo médio gasto no trânsito;
